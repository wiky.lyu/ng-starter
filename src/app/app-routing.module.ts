import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './pages/main/login-page/login-page.component';
import { MainPageComponent } from './pages/main/main-page/main-page.component';
import { MePageComponent } from './pages/main/me-page/me-page.component';
import { RootPageComponent } from './pages/main/root-page/root-page.component';
import { DepartmentListPageComponent } from './pages/staff/department-list-page/department-list-page.component';
import { RoleListPageComponent } from './pages/staff/role-list-page/role-list-page.component';
import { StaffListPageComponent } from './pages/staff/staff-list-page/staff-list-page.component';
import { APIListPageComponent } from './pages/sys/api-list-page/api-list-page.component';
import { PermissionListPageComponent } from './pages/sys/permission-list-page/permission-list-page.component';

const routes: Routes = [{
  path: 'login',
  component: LoginPageComponent,
}, {
  path: 'root',
  component: RootPageComponent,
}, {
  path: '',
  component: MainPageComponent,
  children: [{
    path: 'me',
    component: MePageComponent,
  }, {
    path: 'org',
    children: [{
      path: 'staffs',
      component: StaffListPageComponent,
    }, {
      path: 'departments',
      component: DepartmentListPageComponent,
    }, {
      path: 'roles',
      component: RoleListPageComponent,
    }]
  }, {
    path: 'sys',
    children: [{
      path: 'permissions',
      component: PermissionListPageComponent,
    }, {
      path: 'apis',
      component: APIListPageComponent,
    }]
  }]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
