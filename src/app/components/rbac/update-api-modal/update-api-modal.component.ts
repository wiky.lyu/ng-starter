import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-update-api-modal',
    styleUrls: ['./update-api-modal.component.scss'],
    templateUrl: './update-api-modal.component.html'
})
export class UpdateAPIModalComponent implements OnInit, OnChanges {

    @Input() isVisible = false;
    @Output() isVisibleChange = new EventEmitter<boolean>();

    @Output() update = new EventEmitter<any>();
    @Input() data: any = {};

    loading = false;

    constructor(private api: ApiService, private message: NzMessageService) {

    }

    ngOnInit() {

    }

    ngOnChanges() {
        if (this.isVisible) {
            if (!this.data.id) {
                this.data.method = 'GET';
                this.data.path = '/api';
            }
        }
    }

    close() {
        this.isVisible = false;
        this.isVisibleChange.emit(false);
    }

    onOk() {
        if (this.data.id) {
            this.updateAPI();
        } else {
            this.createAPI();
        }
    }

    async createAPI() {
        try {
            this.loading = true;
            const data = this.data;
            const r = await this.api.createAPI(data.method, data.path, data.perm_id);
            if (r.status === 11000) {
                this.message.warning('该接口已经存在');
            } else if (r.status !== 0) {
                this.message.warning('未知错误，请联系管理员');
            } else {
                this.message.success('创建成功');
                this.close();
                this.update.emit(r.data);
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }

    async updateAPI() {
        try {
            this.loading = true;
            const data = this.data;
            const r = await this.api.updateAPI(data.id, data.method, data.path, data.perm_id);
            if (r.status === 11000) {
                this.message.warning('该接口已经存在');
            } else if (r.status !== 0) {
                this.message.warning('未知错误，请联系管理员');
            } else {
                this.message.success('更新成功');
                this.close();
                this.update.emit(r.data);
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }
}