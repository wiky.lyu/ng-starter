import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
    selector: 'app-permission-select',
    styleUrls: ['./permission-select.component.scss'],
    templateUrl: './permission-select.component.html'
})
export class PermissionSelectComponent implements OnInit, OnChanges {


    subscription: Subscription;
    constructor(private api: ApiService, private message: NzMessageService, private notification: NotificationService) {
        this.subscription = this.notification.subscriber$.subscribe((e) => {
            if (e === 'permission') {
                this.findPermissionTree();
            }
        });
    }

    @Input() label = '权限';
    @Input() value = 0;
    @Output() valueChange = new EventEmitter<number>();

    @Input() disabledID = 0;

    nodes: any[] = [];

    ngOnInit() {
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    ngOnChanges() {
        this.findPermissionTree();
    }

    async findPermissionTree() {
        try {
            const r = await this.api.findPermissionTree();
            this.nodes = this.convertNodes(r.data || []);
        } catch (error) {
            this.message.error('网络错误');
        }
        return [];
    }

    onChange(data: any) {
        this.value = this.value || 0;
        this.valueChange.emit(this.value);
    }

    convertNodes(perms: any[], disabled = false) {
        const nodes: any[] = [];
        for (const p of perms) {
            const n: any = {
                title: p.name + '(' + p.fullcode + ')',
                key: p.id,
                data: p,
                children: [],
                isLeaf: p.children.length === 0,
                expanded: true,
                disabled: disabled || p.id === this.disabledID,
            };
            nodes.push(n);
            n.children = this.convertNodes(p.children, n.disabled);
        }
        return nodes;
    }
}
