import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ApiService } from 'src/app/services/api.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
    selector: 'app-update-permission-modal',
    styleUrls: ['./update-permission-modal.component.scss'],
    templateUrl: './update-permission-modal.component.html'
})
export class UpdatePermissionModalComponent implements OnInit {

    @Input() isVisible = false;
    @Output() isVisibleChange = new EventEmitter<boolean>();
    @Output() update = new EventEmitter<boolean>();

    @Input() data: any = {};
    loading = false;

    constructor(private api: ApiService, private message: NzMessageService, private notification: NotificationService) {

    }

    ngOnInit() {

    }

    close() {
        this.isVisible = false;
        this.isVisibleChange.emit(false);
    }

    onOk() {
        if (this.data.id) {
            this.updatePermission();
        } else {
            this.createPermission();
        }
    }

    async updatePermission() {
        try {
            this.loading = true;
            const data = this.data;
            const r = await this.api.updatePermission(data.id, data.name, data.code, data.parent_id, data.sort);
            if (r.status !== 0) {
                this.message.warning('未知错误，请联系管理员');
            } else {
                this.message.success('更新成功');
                this.close();
                this.update.emit(r.data);
                this.notification.emit('permission');
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }

    async createPermission() {
        try {
            this.loading = true;
            const data = this.data;
            const r = await this.api.createPermission(data.name, data.code, data.parent_id, data.sort);
            if (r.status !== 0) {
                this.message.warning('未知错误，请联系管理员');
            } else {
                this.message.success('创建成功');
                this.close();
                this.update.emit(r.data);
                this.notification.emit('permission');
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }
}
