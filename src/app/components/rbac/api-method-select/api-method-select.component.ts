import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-api-method-select',
    styleUrls: ['./api-method-select.component.scss'],
    templateUrl: './api-method-select.component.html'
})
export class APIMethodSelectComponent implements OnInit {

    @Input() value = '';
    @Output() valueChange = new EventEmitter<string>();

    ngOnInit() {

    }

    onChange() {
        this.value = this.value || '';
        this.valueChange.emit(this.value);
    }
}