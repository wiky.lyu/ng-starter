import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-me-avatar-menu',
    styleUrls: ['./me-avatar-menu.component.scss'],
    templateUrl: './me-avatar-menu.component.html'
})
export class MeAvatarMenuComponent implements OnInit {

    data: any = {};

    constructor(private api: ApiService, private message: NzMessageService, private router: Router) {

    }

    ngOnInit() {
        this.getMe();
    }

    async getMe() {
        try {
            const r = await this.api.getMe();
            this.data = r.data;
        } catch (error) {

        }
    }

    async logout() {
        try {
            const r = await this.api.logout();
        } catch (error) {

        } finally {
            this.router.navigateByUrl('/login', { replaceUrl: true });
        }
    }
}
