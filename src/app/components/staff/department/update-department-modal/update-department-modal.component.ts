import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ApiService } from 'src/app/services/api.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
    selector: 'app-update-department-modal',
    styleUrls: ['./update-department-modal.component.scss'],
    templateUrl: './update-department-modal.component.html'
})
export class UpdateDepartmentModalComponent implements OnInit {

    @Input() isVisible = false;
    @Output() isVisibleChange = new EventEmitter<boolean>();
    @Output() update = new EventEmitter<any>();

    @Input() data: any = {};
    loading = false;

    constructor(private api: ApiService, private message: NzMessageService, private notification: NotificationService) {

    }

    ngOnInit() {
    }

    close() {
        this.isVisible = false;
        this.isVisibleChange.emit(false);
    }

    onOk() {
        if (this.data.id) {
            this.updateDepartment();
        } else {
            this.createDepartment();
        }
    }

    async createDepartment() {
        try {
            this.loading = true;
            const data = this.data;
            const r = await this.api.createDepartment(data.name, data.parent_id);
            if (r.status !== 0) {
                this.message.warning('未知错误，请联系管理员');
            } else {
                this.message.success('创建成功');
                this.close();
                this.update.emit(r.data);
                this.notification.emit('department');
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }

    async updateDepartment() {
        try {
            this.loading = true;
            const data = this.data;
            const r = await this.api.updateDepartment(data.id, data.name, data.parent_id);
            if (r.status !== 0) {
                this.message.warning('未知错误，请联系管理员');
            } else {
                this.message.success('更新成功');
                this.close();
                this.update.emit(r.data);
                this.notification.emit('department');
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }
}