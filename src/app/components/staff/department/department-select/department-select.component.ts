import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
    selector: 'app-department-select',
    styleUrls: ['./department-select.component.scss'],
    templateUrl: './department-select.component.html'
})
export class DepartmentSelectComponent implements OnInit, OnChanges {

    @Input() value = 0;
    @Output() valueChange = new EventEmitter<number>();
    @Input() label = "部门"

    subscription: Subscription;
    constructor(private api: ApiService, private message: NzMessageService, private notification: NotificationService) {
        this.subscription = this.notification.subscriber$.subscribe((e) => {
            if (e === 'department') {
                this.findDepartmentTree();
            }
        });
    }

    ngOnInit() {

    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    ngOnChanges() {
        this.findDepartmentTree();
    }

    nodes: any[] = [];
    @Input() disabledID = 0;

    async findDepartmentTree() {
        try {
            const r = await this.api.findDepartmentTree();
            this.nodes = this.convertNodes(r.data || []);
        } catch (error) {
            this.message.error('网络错误');
        }
        return [];
    }

    onChange(data: any) {
        this.value = this.value || 0;
        this.valueChange.emit(this.value);
    }

    convertNodes(perms: any[], disabled = false) {
        const nodes: any[] = [];
        for (const p of perms) {
            const n: any = {
                title: p.name,
                key: p.id,
                data: p,
                children: [],
                isLeaf: p.children.length === 0,
                expanded: true,
                disabled: disabled || p.id === this.disabledID,
            };
            nodes.push(n);
            n.children = this.convertNodes(p.children, n.disabled);
        }
        return nodes;
    }


}