import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-role-select',
    styleUrls: ['./role-select.component.scss'],
    templateUrl: './role-select.component.html'
})
export class RoleSelectComponent implements OnInit {

    value: any = '';
    @Output() valueChange = new EventEmitter<any>();

    constructor(private api: ApiService, private message: NzMessageService) {

    }

    ngOnInit() {
        this.findDepartmentTree();
    }

    onChange(event: any) {
        if (!event) {
            return;
        }
        this.findRole(event, this.nodes);
    }

    findRole(key: any, nodes: any[]): boolean {
        for (const n of nodes) {
            if (n.key === key) {
                this.valueChange.emit(n.data);
                return true;
            }
            if (this.findRole(key, n.children)) {
                return true;
            }
        }
        return false;
    }

    nodes: any[] = [];
    async findDepartmentTree() {
        try {
            const r = await this.api.findDepartmentTree();
            this.nodes = this.convertNodes(r.data || []);
        } catch (error) {
            this.message.error('网络错误');
        }
    }

    convertNodes(departments: any[]) {
        const nodes: any[] = [];
        for (const p of departments) {
            const n: any = {
                title: p.name,
                key: p.id,
                data: p,
                children: [],
                selectable: false,
            };
            nodes.push(n);
            n.children = this.convertNodes(p.children || []);
        }
        return nodes;
    }

    async findDepartmentRoles(departmentID: number) {
        try {
            const r = await this.api.findDepartmentRoles(departmentID);
            const nodes: any[] = [];
            for (const d of r.data) {
                nodes.push({
                    title: d.name,
                    key: 'role.' + d.id,
                    data: d,
                    isLeaf: true,
                })
            }
            return nodes;
        } catch (error) {
            this.message.error('网络错误');
        }
        return [];
    }

    onExpandChange(e: NzFormatEmitEvent): void {
        const node = e.node;
        if (node && node.getChildren().length === 0 && node.isExpanded) {
            this.findDepartmentRoles(node.origin.data.id).then(data => {
                node.addChildren(data);
            });
        }
    }
}