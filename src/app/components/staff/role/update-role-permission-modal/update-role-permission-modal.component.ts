import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-update-role-permission-modal',
    styleUrls: ['./update-role-permission-modal.component.scss'],
    templateUrl: './update-role-permission-modal.component.html'
})
export class UpdateRolePermissionModalComponent implements OnInit, OnChanges {

    @Input() isVisible = false;
    @Output() isVisibleChange = new EventEmitter<boolean>();

    @Input() roleID = 0;

    loading = false;

    nodes: any[] = [];

    constructor(private api: ApiService, private message: NzMessageService) {

    }

    ngOnInit() {

    }

    ngOnChanges() {
        if (this.isVisible) {
            this.findRolePermissions();
        } else {
            this.nodes = [];
        }
    }

    close() {
        this.isVisible = false;
        this.isVisibleChange.emit(false);
    }

    async findRolePermissions() {
        this.nodes = [];
        if (!this.roleID) {
            return;
        }
        try {
            this.loading = true;
            const r = await this.api.findRolePermissions(this.roleID);
            this.nodes = this.convertNodes(r.data || []);
            for (const n of this.nodes) {
                n.expanded = false;
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }

    convertNodes(perms: any[]) {
        const nodes: any[] = [];
        for (const p of perms) {
            const n: any = {
                title: p.name,
                key: p.id,
                data: p,
                children: [],
                isLeaf: p.children.length === 0,
                expanded: true,
                checked: p.checked,
            };
            n.children = this.convertNodes(p.children);
            nodes.push(n);
        }
        return nodes;
    }


    onOk() {
        let rps: any[] = [];
        this.findChecked(rps, this.nodes);
        this.updateRolePermissions(rps);
    }

    findChecked(rps: any[], nodes: any[]) {
        for (const n of nodes) {
            if (n.checked) {
                rps.push({ perm_id: n.data.id, scope: n.data.scope });
            }
            this.findChecked(rps, n.children);
        }
    }

    async updateRolePermissions(perms: any[]) {
        try {
            this.loading = true;
            const r = await this.api.updateRolePermissions(this.roleID, perms);
            if (r.status !== 0) {
                this.message.warning('未知错误，请联系管理员')
            } else {
                this.message.success('更新成功');
                this.close();
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }
}