import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-role-permission-scope-select',
    styleUrls: ['./role-permission-scope-select.component.scss'],
    templateUrl: './role-permission-scope-select.component.html'
})
export class RolePermissionScopeSelectComponent implements OnInit {

    @Input() value = 0;
    @Output() valueChange = new EventEmitter<number>();

    @Input() disabled = false;

    ngOnInit() {
    }

    onChange() {
        this.value = this.value || 0;
        this.valueChange.emit(this.value);
    }
}