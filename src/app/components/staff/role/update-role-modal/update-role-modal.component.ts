import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-update-role-modal',
    styleUrls: ['./update-role-modal.component.scss'],
    templateUrl: './update-role-modal.component.html'
})
export class UpdateRoleModalComponent implements OnInit {

    @Input() isVisible = false;
    @Output() isVisibleChange = new EventEmitter<boolean>();
    @Output() update = new EventEmitter<any>();

    loading = false;
    @Input() data: any = {};

    constructor(private api: ApiService, private message: NzMessageService) {

    }

    ngOnInit() {

    }

    close() {
        this.isVisible = false;
        this.isVisibleChange.emit(false);
    }

    onOk() {
        if (this.data.id) {
            this.updateRole();
        } else {
            this.createRole();
        }
    }

    async createRole() {
        try {
            this.loading = true;
            const data = this.data;
            const r = await this.api.createRole(data.name, data.department_id);
            if (r.status !== 0) {
                this.message.warning('未知错误，请联系管理员');
            } else {
                this.message.success('创建成功')
                this.close();
                this.update.emit(r.data);
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }

    async updateRole() {
        try {
            this.loading = true;
            const data = this.data;
            const r = await this.api.updateRole(data.id, data.name, data.department_id);
            if (r.status !== 0) {
                this.message.warning('未知错误，请联系管理员');
            } else {
                this.message.success('更新成功')
                this.close();
                this.update.emit(r.data);
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }
}