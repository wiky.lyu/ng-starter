import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-staff-status',
    styleUrls: ['./staff-status.component.scss'],
    templateUrl: './staff-status.component.html'
})
export class StaffStatusComponent implements OnInit {

    @Input() status = 0;

    ngOnInit() {

    }

    text() {
        const m: any = {
            1: '正常',
            2: '封禁'
        };
        return m[this.status] || '未知';
    }

    color() {
        const m: any = {
            1: 'success',
            2: 'error'
        };
        return m[this.status] || 'default';
    }
}
