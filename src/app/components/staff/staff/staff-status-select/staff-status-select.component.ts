import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-staff-status-select',
    styleUrls: ['./staff-status-select.component.scss'],
    templateUrl: './staff-status-select.component.html'
})
export class StaffStatusSelectComponent implements OnInit {

    @Input() value = 0;
    @Output() valueChange = new EventEmitter<number>();

    constructor() {

    }

    ngOnInit() {

    }

    onChange() {
        this.value = this.value || 0;
        this.valueChange.emit(this.value);
    }
}
