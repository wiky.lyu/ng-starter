import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-update-staff-modal',
    styleUrls: ['./update-staff-modal.component.scss'],
    templateUrl: './update-staff-modal.component.html'
})
export class UpdateStaffModalComponent implements OnInit {

    @Input() isVisible = false;
    @Output() isVisibleChange = new EventEmitter<boolean>();
    @Output() update = new EventEmitter<any>();

    @Input() data: any = {};
    loading = false;

    constructor(private api: ApiService, private message: NzMessageService) {

    }

    ngOnInit() {

    }

    close() {
        this.isVisible = false;
        this.isVisibleChange.emit(false);
    }

    onOk() {
        if (this.data.username.length < 4) {
            this.message.warning('用户名过短');
        } else if (this.data.id) {
            this.updateStaff();
        } else {
            if (!this.data.password || this.data.password.length < 6) {
                this.message.warning('密码过短');
            } else {
                this.createStaff();
            }
        }
    }

    async createStaff() {
        try {
            this.loading = true;
            const data = this.data;
            const r = await this.api.createStaff(data.username, data.name, data.password, data.department_id);
            if (r.status === 10003) {
                this.message.warning('用户名重复');
            } else if (r.status !== 0) {
                this.message.warning('未知错误，请联系管理员');
            } else {
                this.message.success('创建成功');
                this.update.emit(r.data);
                this.close();
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }

    async updateStaff() {
        try {
            this.loading = true;
            const data = this.data;
            const r = await this.api.updateStaff(data.id, data.name, data.password, data.department_id);
            if (r.status !== 0) {
                this.message.warning('未知错误，请联系管理员');
            } else {
                this.message.success('更新成功');
                this.update.emit(r.data);
                this.close();
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }
}
