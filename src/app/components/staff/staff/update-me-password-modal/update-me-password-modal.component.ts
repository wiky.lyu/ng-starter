import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-update-me-password-modal',
    styleUrls: ['./update-me-password-modal.component.scss'],
    templateUrl: './update-me-password-modal.component.html'
})
export class UpdateMePasswordModalComponent implements OnInit {

    @Input() isVisible = false;
    @Output() isVisibleChange = new EventEmitter<boolean>();

    loading = false;

    oldPassword: string = '';
    newPassword1: string = '';
    newPassword2: string = '';

    constructor(private api: ApiService, private message: NzMessageService) {

    }

    ngOnInit() {

    }

    close() {
        this.isVisible = false;
        this.isVisibleChange.emit(false);
    }

    onOk() {
        if (this.newPassword1.length < 6) {
            this.message.warning('新密码长度至少6位');
        } else if (this.newPassword1 != this.newPassword2) {
            this.message.warning('两次新密码输入不匹配');
        } else {
            this.updateMePassword();
        }
    }

    async updateMePassword() {
        try {
            this.loading = true;
            const r = await this.api.updateMePassword(this.oldPassword, this.newPassword2);
            if (r.status === 10001) {
                this.message.warning('原密码错误');
            } else if (r.status !== 0) {
                this.message.warning('未知错误，请联系管理员');
            } else {
                this.close();
                this.message.success('密码修改成功');
                this.oldPassword = '';
                this.newPassword1 = '';
                this.newPassword2 = '';
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }
}