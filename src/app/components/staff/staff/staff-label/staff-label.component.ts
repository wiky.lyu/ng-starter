import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-staff-label',
    styleUrls: ['./staff-label.component.scss'],
    templateUrl: './staff-label.component.html'
})
export class StaffLabelComponent implements OnInit, OnChanges {

    @Input() id = 0;

    data: any = {};
    loading = false;

    constructor(private api: ApiService, private message: NzMessageService) {

    }

    ngOnInit() {

    }

    ngOnChanges() {
        this.getStaff();
    }

    async getStaff() {
        this.data = {};
        if (!this.id) {
            return;
        }
        try {
            this.loading = true;
            const r = await this.api.getStaff(this.id);
            this.data = r.data;
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }
}
