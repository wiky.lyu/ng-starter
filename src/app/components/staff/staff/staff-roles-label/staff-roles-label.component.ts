import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ApiService } from 'src/app/services/api.service';
import { PermissionService } from 'src/app/services/permission.service';
import { removeArrayIndex } from 'src/app/x/slice';

@Component({
    selector: 'app-staff-roles-label',
    styleUrls: ['./staff-roles-label.component.scss'],
    templateUrl: './staff-roles-label.component.html'
})
export class StaffRolesLabelComponent implements OnInit, OnChanges {

    @Input() staffID = 0;
    roles: any[] = [];

    label = '';
    loading = false;
    constructor(private api: ApiService, private message: NzMessageService, private permission: PermissionService) {

    }

    ngOnInit() {
        this.checkPermissions();
    }

    ngOnChanges() {
        this.findStaffRoles();
    }

    perms: any = {}
    async checkPermissions() {
        this.perms = await this.permission.check(['org.staff.set-roles']);
    }

    async findStaffRoles() {
        this.roles = [];
        this.label = '*';
        if (!this.staffID) {
            return;
        }
        try {
            const r = await this.api.findStaffRoles(this.staffID);
            this.roles = r.data || [];
            const names = [];
            for (const r of this.roles) {
                names.push(r.name);
            }
            if (names.length > 0) {
                this.label = names.join(';');
            }
        } catch (error) {

        }
    }

    isModalVisible = false;
    showModal() {
        if (this.perms['org.staff.set-roles']) {
            this.isModalVisible = true;
        }
    }

    closeModel() {
        this.isModalVisible = false;
    }

    onRoleSelect(role: any) {
        if (!role) {
            return;
        }
        const roles: any[] = [];
        for (const r of this.roles) {
            if (r.id === role.id) {
                return;
            }
            roles.push(r);
        }
        roles.push(role);
        this.roles = roles;
    }

    removeRole(i: number) {
        this.roles = removeArrayIndex(this.roles, i);
    }

    onOk() {
        const roleIDs: number[] = [];
        for (const r of this.roles) {
            roleIDs.push(r.id);
        }
        this.updateStaffRoles(roleIDs);
    }

    async updateStaffRoles(roleIDs: number[]) {
        try {
            this.loading = true;
            const r = await this.api.updateStaffRoles(this.staffID, roleIDs);
            if (r.status !== 0) {
                this.message.warning('未知错误，请联系管理员');
            } else {
                this.message.success('更新成功');
                this.closeModel();
                this.findStaffRoles();
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }
}