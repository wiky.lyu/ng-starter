import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
    selector: 'app-datetime',
    templateUrl: './datetime.component.html',
    styleUrls: ['./datetime.component.scss']
})
export class DatetimeComponent implements OnInit, OnChanges {

    @Input() text: any;
    @Input() time = true;

    datetime: Date = new Date(0);

    constructor() { }

    init(): void {
        if (typeof this.text === 'string') {
            this.datetime = new Date(this.text);
        } else if (typeof this.text === 'number') {
            this.datetime = new Date(this.text * 1000);
        }
    }

    ngOnInit(): void {
    }

    ngOnChanges() {
        this.init();
    }


}
