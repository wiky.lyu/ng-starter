import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-input',
    styleUrls: ['./input.component.scss'],
    templateUrl: './input.component.html'
})
export class InputComponent implements OnInit {

    @Input() label = '';
    @Input() value = '';
    @Output() valueChange = new EventEmitter<string>();
    @Output() enter = new EventEmitter<boolean>();
    @Input() placeholder = '';
    @Input() disabled = false;
    @Input() options: any = [];
    @Input() password = false;

    @Input() block = false;

    ngOnInit() {

    }

    onChange() {
        const v = this.value || '';
        this.valueChange.emit(v);
    }

    onEnter() {
        this.enter.emit(true);
    }
}
