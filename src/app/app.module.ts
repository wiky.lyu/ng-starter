import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { zh_CN } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import zh from '@angular/common/locales/zh';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzStatisticModule } from 'ng-zorro-antd/statistic';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzTreeModule } from 'ng-zorro-antd/tree';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzTreeSelectModule } from 'ng-zorro-antd/tree-select';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzTimelineModule } from 'ng-zorro-antd/timeline';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzNotificationServiceModule } from 'ng-zorro-antd/notification';
import { IconsProviderModule } from './icons-provider.module';
import { LoginPageComponent } from './pages/main/login-page/login-page.component';
import { ApiInterceptor } from './services/api.interceptor';
import { MainPageComponent } from './pages/main/main-page/main-page.component';
import { MeAvatarMenuComponent } from './components/staff/me-avatar-menu/me-avatar-menu.component';
import { RootPageComponent } from './pages/main/root-page/root-page.component';
import { StaffListPageComponent } from './pages/staff/staff-list-page/staff-list-page.component';
import { InputComponent } from './components/elements/input/input.component';
import { StaffStatusSelectComponent } from './components/staff/staff/staff-status-select/staff-status-select.component';
import { DatetimeComponent } from './components/elements/datetime/datetime.component';
import { StaffLabelComponent } from './components/staff/staff/staff-label/staff-label.component';
import { StaffStatusComponent } from './components/staff/staff/staff-status/staff-status.component';
import { UpdateStaffModalComponent } from './components/staff/staff/update-staff-modal/update-staff-modal.component';
import { PermissionListPageComponent } from './pages/sys/permission-list-page/permission-list-page.component';
import { UpdatePermissionModalComponent } from './components/rbac/update-permission-modal/update-permission-modal.component';
import { PermissionSelectComponent } from './components/rbac/permission-select/permission-select.component';
import { APIMethodSelectComponent } from './components/rbac/api-method-select/api-method-select.component';
import { APIListPageComponent } from './pages/sys/api-list-page/api-list-page.component';
import { UpdateAPIModalComponent } from './components/rbac/update-api-modal/update-api-modal.component';
import { DepartmentListPageComponent } from './pages/staff/department-list-page/department-list-page.component';
import { UpdateDepartmentModalComponent } from './components/staff/department/update-department-modal/update-department-modal.component';
import { DepartmentSelectComponent } from './components/staff/department/department-select/department-select.component';
import { RoleListPageComponent } from './pages/staff/role-list-page/role-list-page.component';
import { UpdateRoleModalComponent } from './components/staff/role/update-role-modal/update-role-modal.component';
import { UpdateRolePermissionModalComponent } from './components/staff/role/update-role-permission-modal/update-role-permission-modal.component';
import { RolePermissionScopeSelectComponent } from './components/staff/role/role-permission-scope-select/role-permission-scope-select.component';
import { StaffRolesLabelComponent } from './components/staff/staff/staff-roles-label/staff-roles-label.component';
import { RoleSelectComponent } from './components/staff/role/role-select/role-select.component';
import { MePageComponent } from './pages/main/me-page/me-page.component';
import { UpdateMePasswordModalComponent } from './components/staff/staff/update-me-password-modal/update-me-password-modal.component';

registerLocaleData(zh);

@NgModule({
  declarations: [
    AppComponent,

    LoginPageComponent,
    MainPageComponent,
    MeAvatarMenuComponent,
    RootPageComponent,
    StaffListPageComponent,
    InputComponent,
    StaffStatusSelectComponent,
    DatetimeComponent,
    StaffLabelComponent,
    StaffStatusComponent,
    UpdateStaffModalComponent,
    PermissionListPageComponent,
    UpdatePermissionModalComponent,
    PermissionSelectComponent,
    APIMethodSelectComponent,
    APIListPageComponent,
    UpdateAPIModalComponent,
    DepartmentListPageComponent,
    UpdateDepartmentModalComponent,
    DepartmentSelectComponent,
    RoleListPageComponent,
    UpdateRoleModalComponent,
    UpdateRolePermissionModalComponent,
    RolePermissionScopeSelectComponent,
    StaffRolesLabelComponent,
    RoleSelectComponent,
    MePageComponent,
    UpdateMePasswordModalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,

    IconsProviderModule,

    NzFormModule,
    NzPaginationModule,
    NzBreadCrumbModule,
    NzDatePickerModule,
    NzStatisticModule,
    NzDescriptionsModule,
    NzLayoutModule,
    NzFormModule,
    NzMessageModule,
    NzPopconfirmModule,
    NzPopoverModule,
    NzButtonModule,
    NzTabsModule,
    NzTableModule,
    NzMenuModule,
    NzSelectModule,
    NzInputModule,
    NzInputNumberModule,
    NzDropDownModule,
    NzAvatarModule,
    NzModalModule,
    NzToolTipModule,
    NzBadgeModule,
    NzPopoverModule,
    NzIconModule,
    NzDividerModule,
    NzSpinModule,
    NzCheckboxModule,
    NzTreeModule,
    NzTreeSelectModule,
    NzListModule,
    NzSwitchModule,
    NzCollapseModule,
    NzUploadModule,
    NzTagModule,
    NzAutocompleteModule,
    NzTimelineModule,
    NzRadioModule,
    NzNotificationServiceModule,
    NzCardModule,
  ],
  providers: [{ provide: NZ_I18N, useValue: zh_CN }, {
    provide: HTTP_INTERCEPTORS,
    useClass: ApiInterceptor,
    multi: true,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
