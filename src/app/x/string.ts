

export function randomString(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

export function parseStringArray(s: string) {
    if (!s) {
        return [];
    }
    return s.split(',');
}


export function getLines(text) {
    const lines = text.split('\n');
    const codes = [];
    for (const line of lines) {
        const l = line.trim();
        if (l) {
            codes.push(l);
        }
    }

    return codes;
}

export function getTextLineCount(text) {
    const seps = text.split('\n');
    let c = 0;
    for (const sep of seps) {
        if (sep.trim()) {
            c++;
        }
    }
    return c;
}
