import { ActivatedRoute, Params, Router } from '@angular/router';
import { parseDateRangesWithDefault } from './datetime';
import { parseIntWithDefault } from './int';

export function mergeRouter(router: Router, route: ActivatedRoute, params: Params) {
    params._t = new Date().getTime();
    router.navigate(
        [],
        {
            relativeTo: route,
            queryParams: params,
            queryParamsHandling: 'merge',
        });
}

export function parseRouterQuery(route: ActivatedRoute, key: string) {
    return route.snapshot.queryParamMap.get(key) || '';
}

export function parseIntQuery(route: ActivatedRoute, key: string, def: number) {
    const v = parseRouterQuery(route, key);
    return parseIntWithDefault(v, def);
}

export function parseStringQuery(route: ActivatedRoute, key: string, def: string = '') {
    return parseRouterQuery(route, key) || def;
}

export function parseDateRangesQuery(route: ActivatedRoute, key: string, def: any = []) {
    const v = parseRouterQuery(route, key);
    return parseDateRangesWithDefault(v, def);
}
