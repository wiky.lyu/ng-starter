import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
    providedIn: 'root'
})
export class PermissionService {

    perms: any = {};

    constructor(private api: ApiService) {
    }

    reset() {
        this.perms = {};
    }

    async check(codes: string[]) {
        const res: any = {};
        const tofetch = [];
        for (const code of codes) {
            if (this.perms[code] === true || this.perms[code] === false) {
                res[code] = this.perms[code];
            } else {
                tofetch.push(code);
            }
        }

        /* 不需要查询权限 */
        if (tofetch.length === 0) {
            return res;
        }

        try {
            const r = await this.api.findMePermissions(tofetch);
            const data = r.data || [];
            const perms: any = {};
            for (const d of data) {
                perms[d.fullcode] = true;
            }
            this.perms = Object.assign(this.perms, perms);
            return Object.assign(res, perms);
        } catch (error) {
            return {};
        }
    }
}