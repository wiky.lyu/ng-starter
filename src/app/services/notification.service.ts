import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class NotificationService {
    observer = new Subject();
    public subscriber$ = this.observer.asObservable();

    constructor() {

    }

    emit(data: any) {
        this.observer.next(data);
    }
}
