import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    constructor(private http: HttpClient) {
    }

    fetchMap: any = {};

    buildurl(path: string, queryMap: any = null): string {
        let query = '';
        if (queryMap) {
            const queries = [];
            for (const k of Object.keys(queryMap)) {
                const v = queryMap[k];
                if (v instanceof Array) {
                    for (const vv of v) {
                        queries.push(k + '=' + encodeURIComponent(vv));
                    }
                } else {
                    queries.push(k + '=' + encodeURIComponent(queryMap[k]));
                }
            }
            query = '?' + queries.join('&');
        }
        return environment.apiHost + path + query;
    }

    put(url: string, body: any): Promise<any> {
        return this.http.put(url, body, { withCredentials: true }).toPromise() as Promise<any>;
    }

    post(url: string, body: any): Promise<any> {
        return this.http.post(url, body, { withCredentials: true }).toPromise() as Promise<any>;
    }

    get(url: string): Promise<any> {
        return this.http.get(url, { withCredentials: true }).toPromise() as Promise<any>;
    }

    delete(url: string): Promise<any> {
        return this.http.delete(url, { withCredentials: true }).toPromise() as Promise<any>;
    }

    login(username: string, password: string) {
        const url = this.buildurl('/login');
        const body = {
            username,
            password,
        };
        return this.put(url, body);
    }

    logout() {
        const url = this.buildurl('/logout');
        return this.put(url, null);
    }

    getMe() {
        const url = this.buildurl('/me');
        return this.once(url);
    }

    once(url: string) {
        return this.fetch(url, () => this.get(url));
    }

    reset() {
        this.fetchMap = {};
    }
    async fetch(name: string, func: () => Promise<any>) {
        try {
            let doing = this.fetchMap[name];
            if (!doing) {
                doing = func();
                this.fetchMap[name] = doing;
            }
            const r = await doing;
            return r;
        } catch (error) {
            throw error;
        } finally {
            delete (this.fetchMap[name]);
        }
    }

    getAppInfo() {
        const url = this.buildurl('/system/appinfo');
        return this.once(url);
    }

    createRootStaff(username: string, name: string, password: string) {
        const url = this.buildurl('/system/root');
        const body = {
            username,
            name,
            password,
        };
        return this.post(url, body);
    }

    findStaffs(query: string, status: number, page: number, pageSize: number) {
        const url = this.buildurl('/staffs', { query, status, page, page_size: pageSize });
        return this.get(url);
    }

    getStaff(id: number) {
        const url = this.buildurl('/staff/' + id);
        return this.once(url);
    }

    createStaff(username: string, name: string, password: string, departmentID: number) {
        const url = this.buildurl('/staff');
        const body = {
            username,
            name,
            password,
            department_id: departmentID,
        };
        return this.post(url, body);
    }

    updateStaff(id: number, name: string, password: string, departmentID: number) {
        const url = this.buildurl('/staff/' + id);
        const body = {
            name,
            password,
            department_id: departmentID,
        };
        return this.put(url, body);
    }

    findPermissions(query: string, parentID: number, page: number, pageSize: number) {
        const url = this.buildurl('/rbac/permissions', { query, parent_id: parentID, page, page_size: pageSize });
        return this.once(url);
    }

    findPermissionTree() {
        const url = this.buildurl('/rbac/permission/tree');
        return this.once(url);
    }

    createPermission(name: string, code: string, parentID: number, sort: number) {
        const url = this.buildurl('/rbac/permission');
        const body = {
            name, code, parent_id: parentID, sort
        };
        return this.post(url, body);
    }

    updatePermission(id: number, name: string, code: string, parentID: number, sort: number) {
        const url = this.buildurl('/rbac/permission/' + id);
        const body = {
            name, code, parent_id: parentID, sort,
        };
        return this.put(url, body);
    }

    findAPIs(query: string, method: string, page: number, pageSize: number) {
        const url = this.buildurl('/rbac/apis', { query, method, page, page_size: pageSize });
        return this.once(url);
    }

    createAPI(method: string, path: string, permID: number) {
        const url = this.buildurl('/rbac/api');
        const body = {
            method,
            path,
            perm_id: permID,
        };
        return this.post(url, body);
    }

    updateAPI(id: number, method: string, path: string, permID: number) {
        const url = this.buildurl('/rbac/api/' + id);
        const body = {
            method,
            path,
            perm_id: permID,
        };
        return this.put(url, body);
    }

    findDepartmentTree() {
        const url = this.buildurl('/department/tree');
        return this.once(url);
    }

    createDepartment(name: string, parentID: number) {
        const url = this.buildurl('/department');
        const body = {
            name,
            parent_id: parentID,
        };
        return this.post(url, body);
    }

    updateDepartment(id: number, name: string, parentID: number) {
        const url = this.buildurl('/department/' + id);
        const body = {
            name,
            parent_id: parentID,
        };
        return this.put(url, body);
    }

    findRoles(query: string, departmentID: number, page: number, pageSize: number) {
        const url = this.buildurl('/roles', { query, department_id: departmentID, page, page_size: pageSize });
        return this.once(url);
    }

    createRole(name: string, departmentID: number) {
        const url = this.buildurl('/role');
        const body = {
            name,
            department_id: departmentID,
        };
        return this.post(url, body);
    }

    updateRole(id: number, name: string, departmentID: number) {
        const url = this.buildurl('/role/' + id);
        const body = {
            name,
            department_id: departmentID,
        };
        return this.put(url, body);
    }

    findRolePermissions(id: number) {
        const url = this.buildurl('/role/' + id + '/permissions');
        return this.once(url);
    }

    updateRolePermissions(id: number, perms: any[]) {
        const url = this.buildurl('/role/' + id + '/permissions');
        const body = {
            perms
        };
        return this.put(url, body);
    }

    findStaffRoles(id: number) {
        const url = this.buildurl('/staff/' + id + '/roles');
        return this.once(url);
    }

    findDepartmentRoles(id: number) {
        const url = this.buildurl('/department/' + id + '/roles');
        return this.once(url);
    }

    updateStaffRoles(id: number, roleIDs: number[]) {
        const url = this.buildurl('/staff/' + id + '/roles');
        const body = { role_ids: roleIDs };
        return this.put(url, body);
    }

    findMeMenus() {
        const url = this.buildurl('/me/menus');
        return this.once(url);
    }

    findMePermissions(codes: string[]) {
        const url = this.buildurl('/me/perms', { codes });
        return this.once(url);
    }

    updateMePassword(old: string, n: string) {
        const url = this.buildurl('/me/password');
        const body = { old, new: n };
        return this.put(url, body);
    }
}

