import { Component, OnInit } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { NzMessageService } from "ng-zorro-antd/message";
import { ApiService } from "src/app/services/api.service";
import { mergeRouter, parseIntQuery, parseStringQuery } from "src/app/x/router";

@Component({
    selector: 'app-api-list-page',
    styleUrls: ['./api-list-page.component.scss'],
    templateUrl: './api-list-page.component.html'
})
export class APIListPageComponent implements OnInit {

    query: string = '';
    queryMethod: string = '';
    page = 1;
    pageSize = 10;
    total = 0;
    apis: any[] = [];
    loading = false;

    constructor(private api: ApiService, private message: NzMessageService, private title: Title,
        private router: Router, private route: ActivatedRoute) {
        this.title.setTitle('系统管理 - 接口列表')

        this.route.queryParams.subscribe(() => {
            this.loadRouter();
            this.findAPIs();
        });
    }

    ngOnInit() {

    }

    loadRouter() {
        this.query = parseStringQuery(this.route, 'query', '');
        this.queryMethod = parseStringQuery(this.route, 'method', '');
        this.page = parseIntQuery(this.route, 'page', 1);
        this.pageSize = parseIntQuery(this.route, 'page_size', 10);
    }

    mergeRouter() {
        mergeRouter(this.router, this.route, {
            query: this.query,
            method: this.queryMethod,
            page: this.page,
            page_size: this.pageSize,
        });
    }

    search() {
        this.page = 1;
        this.mergeRouter();
    }

    onPageChange() {
        this.mergeRouter();
    }

    async findAPIs() {
        try {
            this.loading = true;
            const r = await this.api.findAPIs(this.query, this.queryMethod, this.page, this.pageSize);
            const data = r.data;
            this.page = data.page;
            this.pageSize = data.page_size;
            this.apis = data.list;
            this.total = data.total;
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }

    isUpdateModalVisible = false;
    updateData = {};
    showUpdateModal(data: any) {
        this.isUpdateModalVisible = true;
        this.updateData = Object.assign({}, data);
    }

    showNewModal() {
        this.isUpdateModalVisible = true;
        this.updateData = {};
    }
}