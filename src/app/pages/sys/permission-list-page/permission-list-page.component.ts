import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-permission-list-page',
    styleUrls: ['./permission-list-page.component.scss'],
    templateUrl: './permission-list-page.component.html'
})
export class PermissionListPageComponent implements OnInit {

    constructor(private api: ApiService, private message: NzMessageService, private title: Title) {
        this.title.setTitle('系统设置 - 权限管理');
    }

    nodes: any[] = [];

    isModalVisible = false;
    permData: any = {};

    ngOnInit() {
        this.findPermissionTree();
    }

    convertNodes(perms: any[]) {
        const nodes: any[] = [];
        for (const p of perms) {
            const n: any = {
                title: p.name + '(' + p.fullcode + ')',
                key: p.id,
                data: p,
                children: [],
                isLeaf: p.children.length === 0,
                expanded: this.expandedKeys[p.id],
            };
            nodes.push(n);
            n.children = this.convertNodes(p.children);
        }
        return nodes;
    }

    expandedKeys: any = {};

    async findPermissionTree() {
        try {
            const r = await this.api.findPermissionTree();
            this.nodes = this.convertNodes(r.data || []);
        } catch (error) {
            this.message.error('网络错误');
        }
        return [];
    }

    findExpandedKeys(nodes: any[]) {
        for (const n of nodes) {
            if (n.expanded) {
                this.expandedKeys[n.key] = true;
            }
            this.findExpandedKeys(n.children || []);
        }
    }

    onExpanded(event: any) {
        this.expandedKeys = {};
        this.findExpandedKeys(this.nodes);
    }


    nzEvent(event: NzFormatEmitEvent): void {
        if (event.eventName === 'click') {
            this.isModalVisible = true;
            this.permData = Object.assign({}, event.node?.origin?.data);
        }
    }
    showNewModal() {
        this.isModalVisible = true;
        this.permData = {};
    }
}
