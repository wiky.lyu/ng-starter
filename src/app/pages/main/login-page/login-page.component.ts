import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

    validateForm: FormGroup = this.fb.group({
        userName: [null, [Validators.required, Validators.minLength(6)]],
        password: [null, [Validators.required]],
    });

    loading = false;

    constructor(private fb: FormBuilder, private api: ApiService, private message: NzMessageService,
                private router: Router, private title: Title) {
        this.title.setTitle('XXX管理平台 - 登录');
    }

    submitForm(): void {
        for (const i of Object.keys(this.validateForm.controls)) {
            this.validateForm.controls[i].markAsDirty();
            this.validateForm.controls[i].updateValueAndValidity();
        }
        for (const i of Object.keys(this.validateForm.controls)) {
            if (!this.validateForm.controls[i].valid) {
                return;
            }
        }

        const userName = this.validateForm.get('userName');
        const password = this.validateForm.get('password');
        if (userName && password) {
            this.login(userName.value, password.value);
        }
    }

    ngOnInit() {
        this.getAppInfo();
    }

    async login(username: string, password: string) {
        try {
            this.loading = true;
            const r = await this.api.login(username, password);
            if (r.status === 10000) {
                this.message.warning('用户不存在');
            } else if (r.status === 10001) {
                this.message.warning('密码错误');
            } else if (r.status === 10004) {
                this.message.warning('禁止登录');
            } else if (r.status !== 0) {
                this.message.warning('未知错误');
            } else {
                this.router.navigateByUrl('/', { replaceUrl: true });
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }

    async getAppInfo() {
        try {
            const r = await this.api.getAppInfo();
            if (!r.data.has_root) {
                this.router.navigateByUrl('/root', { replaceUrl: true });
            }
        } catch (error) {

        }
    }
}
