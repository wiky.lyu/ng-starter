import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ApiService } from 'src/app/services/api.service';
import { PermissionService } from 'src/app/services/permission.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-main-page',
    styleUrls: ['./main-page.component.scss'],
    templateUrl: './main-page.component.html'
})
export class MainPageComponent implements OnInit {

    production = environment.production;
    isCollapsed = false;
    v = new Date().getTime();

    me: any = {};
    menus: any = {};

    constructor(private title: Title, private api: ApiService, private permission: PermissionService) {
        this.title.setTitle('欢迎');
    }

    ngOnInit() {
        this.permission.reset();
        this.api.reset();
        this.getMe();
        this.findMeMenu();
    }

    async getMe() {
        try {
            const r = await this.api.getMe();
            this.me = r.data;
        } catch (error) {
        }
    }

    async findMeMenu() {
        this.menus = {};
        try {
            const r = await this.api.findMeMenus();
            const perms = r.data;
            for (const p of perms) {
                this.menus[p.fullcode] = true;
            }
        } catch (error) {

        }
    }
}
