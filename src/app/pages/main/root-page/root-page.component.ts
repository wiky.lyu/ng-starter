import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-root-page',
    styleUrls: ['./root-page.component.scss'],
    templateUrl: './root-page.component.html'
})
export class RootPageComponent implements OnInit {

    constructor(private api: ApiService, private message: NzMessageService,
                private title: Title, private router: Router) {
        this.title.setTitle('系统管理 - 设置管理员帐号');
    }

    data: any = {};
    loading = false;

    ngOnInit() {
        this.getAppInfo();
    }

    async getAppInfo() {
        try {
            const r = await this.api.getAppInfo();
            if (r.data.has_root) {
                this.router.navigateByUrl('/login', { replaceUrl: true });
            }
        } catch (error) {

        }
    }

    submit() {
        if (this.data.username.length < 4) {
            this.message.warning('用户名过短');
        } else if (this.data.password.length < 6) {
            this.message.warning('密码过短');
        } else if (this.data.password != this.data.password2) {
            this.message.warning('两次密码输入不匹配');
        } else {
            this.createRootStaff();
        }
    }

    async createRootStaff() {
        try {
            this.loading = true;
            const data = this.data;
            const r = await this.api.createRootStaff(data.username, data.name, data.password);
            if (r.status === 10005) {
                this.message.warning('管理员帐号已存在，不能重复创建');
            } else if (r.status !== 0) {
                this.message.warning('未知错误');
            } else {
                this.message.success('创建成功，请使用该帐号登录');
                this.router.navigateByUrl('/login', { replaceUrl: true });
            }
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }
}
