import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-me-page',
    styleUrls: ['./me-page.component.scss'],
    templateUrl: './me-page.component.html'
})
export class MePageComponent implements OnInit {

    constructor(private api: ApiService, private message: NzMessageService, private title: Title) {
        this.title.setTitle('个人信息');
    }

    me: any = {};
    ngOnInit() {
        this.getMe();
    }

    isPasswordModalVisible = false;

    async getMe() {
        try {
            const r = await this.api.getMe();
            this.me = r.data;
        } catch (error) {
            this.message.error('网络错误');
        }
    }

    showPasswordModal() {
        this.isPasswordModalVisible = true;
    }
}