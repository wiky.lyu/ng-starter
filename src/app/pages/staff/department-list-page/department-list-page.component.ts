import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';
import { ApiService } from 'src/app/services/api.service';
import { PermissionService } from 'src/app/services/permission.service';

@Component({
    selector: 'app-department-list-page',
    styleUrls: ['./department-list-page.component.scss'],
    templateUrl: './department-list-page.component.html'
})
export class DepartmentListPageComponent implements OnInit {

    loading = false;

    nodes: any[] = [];
    perms: any = {};

    constructor(private api: ApiService, private message: NzMessageService, private title: Title, private permission: PermissionService) {
        this.title.setTitle('组织管理 - 部门管理')
    }

    ngOnInit() {
        this.findDepartments();
        this.checkPermissions();
    }

    async checkPermissions() {
        this.perms = await this.permission.check(['org.department.create', 'org.department.update']);
    }

    async findDepartments() {
        try {
            this.loading = true;
            const r = await this.api.findDepartmentTree();
            this.nodes = this.convertNodes(r.data || [])
                ;
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }

    convertNodes(perms: any[]) {
        const nodes: any[] = [];
        for (const p of perms) {
            const n: any = {
                title: p.name,
                key: p.id,
                data: p,
                children: [],
                isLeaf: p.children.length === 0,
                expanded: true,
            };
            nodes.push(n);
            n.children = this.convertNodes(p.children);
        }
        return nodes;
    }

    isUpdateModalVisible = false;
    updateData = {};
    showNewModal() {
        this.isUpdateModalVisible = true;
        this.updateData = {};
    }

    showUpdateModal(data: any) {
        this.isUpdateModalVisible = true;
        this.updateData = Object.assign({}, data);
    }

    nzEvent(event: NzFormatEmitEvent): void {
        if (event.eventName === 'click' && this.perms['org.department.update']) {
            this.showUpdateModal(event.node?.origin.data);
        }
    }

}