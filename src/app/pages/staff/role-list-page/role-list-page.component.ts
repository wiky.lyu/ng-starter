import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ApiService } from 'src/app/services/api.service';
import { PermissionService } from 'src/app/services/permission.service';
import { mergeRouter, parseIntQuery, parseStringQuery } from 'src/app/x/router';

@Component({
    selector: 'app-role-list-page',
    styleUrls: ['./role-list-page.component.scss'],
    templateUrl: './role-list-page.component.html'
})
export class RoleListPageComponent implements OnInit {

    query = '';
    queryDepartmentID = 0;
    page = 1;
    pageSize = 10;
    total = 0;
    roles: any[] = [];
    loading = false;

    constructor(private api: ApiService, private message: NzMessageService, private title: Title,
        private router: Router, private route: ActivatedRoute, private permission: PermissionService) {
        this.title.setTitle('组织管理 - 角色列表');

        this.route.queryParams.subscribe(() => {
            this.loadRouter();
            this.findRoles();
        })
    }

    ngOnInit() {
        this.checkPermissions();
    }

    perms: any = {};
    async checkPermissions() {
        this.perms = await this.permission.check(['org.role.create', 'org.role.update', 'org.role.set-permissions']);
    }

    loadRouter() {
        this.query = parseStringQuery(this.route, 'query', '');
        this.queryDepartmentID = parseIntQuery(this.route, 'department_id', 0);
        this.page = parseIntQuery(this.route, 'page', 1);
        this.pageSize = parseIntQuery(this.route, 'page_size', 10);
    }

    mergeRouter() {
        mergeRouter(this.router, this.route, {
            query: this.query,
            department_id: this.queryDepartmentID,
            page: this.page,
            page_size: this.pageSize,
        });
    }

    search() {
        this.page = 1;
        this.mergeRouter();
    }

    onPageChange() {
        this.mergeRouter();
    }

    async findRoles() {
        try {
            this.loading = true;
            const r = await this.api.findRoles(this.query, this.queryDepartmentID, this.page, this.pageSize);
            const data = r.data;
            this.page = data.page;
            this.pageSize = data.page_size;
            this.total = data.total;
            this.roles = data.list;
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }

    isUpdateModalVisible = false;
    updateData: any = {};
    showNewModal() {
        this.isUpdateModalVisible = true;
        this.updateData = {};
    }

    showUpdateModal(data: any) {
        this.isUpdateModalVisible = true;
        this.updateData = Object.assign({}, data);
    }

    isPermissionModalVisible = false;
    permissionData: any = {};
    showUpdatePermissionModal(data: any) {
        this.isPermissionModalVisible = true;
        this.permissionData = Object.assign({}, data);
    }
}