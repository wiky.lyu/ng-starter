import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ApiService } from 'src/app/services/api.service';
import { PermissionService } from 'src/app/services/permission.service';
import { mergeRouter, parseIntQuery, parseStringQuery } from 'src/app/x/router';

@Component({
    selector: 'app-staff-list-page',
    styleUrls: ['./staff-list-page.component.scss'],
    templateUrl: './staff-list-page.component.html'
})
export class StaffListPageComponent implements OnInit {

    constructor(private api: ApiService, private message: NzMessageService,
        private title: Title, private router: Router, private route: ActivatedRoute,
        private permission: PermissionService) {
        this.title.setTitle('组织管理 - 人员列表');

        this.route.queryParams.subscribe(() => {
            this.loadRouter();
            this.findStaffs();
        });
    }

    query = '';
    queryStatus = 0;
    page = 1;
    pageSize = 10;
    total = 0;
    loading = false;

    staffs: any[] = [];

    isUpdateModalVisible = false;
    updateData = {};
    ngOnInit() {
        this.checkPermissions();
    }

    perms: any = {};
    async checkPermissions() {
        this.perms = await this.permission.check(['org.staff.create', 'org.staff.update'])
    }

    search() {
        this.page = 1;
        this.mergeRouter();
    }

    onPageChange() {
        this.mergeRouter();
    }

    mergeRouter() {
        mergeRouter(this.router, this.route, {
            query: this.query,
            status: this.queryStatus,
            page: this.page,
            page_size: this.pageSize,
        });
    }

    loadRouter() {
        this.query = parseStringQuery(this.route, 'query', '');
        this.queryStatus = parseIntQuery(this.route, 'status', 0);
        this.page = parseIntQuery(this.route, 'page', 1);
        this.pageSize = parseIntQuery(this.route, 'page_size', 10);
    }

    async findStaffs() {
        try {
            this.loading = true;
            const r = await this.api.findStaffs(this.query, this.queryStatus, this.page, this.pageSize);
            const data = r.data;
            this.page = data.page;
            this.pageSize = data.page_size;
            this.total = data.total;
            this.staffs = data.list;
        } catch (error) {
            this.message.error('网络错误');
        } finally {
            this.loading = false;
        }
    }
    showNewModal() {
        this.isUpdateModalVisible = true;
        this.updateData = {};
    }

    showUpdateModal(data: any) {
        this.isUpdateModalVisible = true;
        this.updateData = Object.assign({}, data);
    }
}
